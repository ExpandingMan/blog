\documentclass[10pt,aspectratio=169]{beamer}

\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hwemoji}

\usepackage{tikz}
\usetikzlibrary{quantikz}
\usetikzlibrary{arrows,shapes,positioning}

\graphicspath{ {./images/} }

%https://github.com/walmes/Tikz for useful tikz examples

\tikzstyle{tbox} = [    
rectangle,
rounded corners,
draw = draculabg,
fill = draculacomment,
text width = 8em,
text centered,
]
\tikzstyle{line} = [draw=draculafg, -latex']


\usetheme{Boadilla}
\usecolortheme{dracula}
\usefonttheme{serif}

\title{}
\subtitle{Quantum Computers Are Not Analog Classical Computers}
\author{Mike Savastio}
\date{}

\newcommand{\cyan}[1]{{\color{draculacyan}#1}}
\newcommand{\green}[1]{{\color{draculagreen}#1}}
\newcommand{\orange}[1]{{\color{draculaorange}#1}}
\newcommand{\pink}[1]{{\color{draculapink}#1}}
\newcommand{\purple}[1]{{\color{draculapurple}#1}}
\newcommand{\red}[1]{{\color{draculared}#1}}
\newcommand{\yellow}[1]{{\color{draculayellow}#1}}
\newcommand{\comment}[1]{{\color{draculacomment}#1}}

\newcommand{\blochsphere}[1]{
    \scalebox{#1}{\begin{tikzpicture}[line cap=round, line join=round]
      \clip(-2.19,-2.49) rectangle (2.66,2.58);
      \draw [shift={(0,0)}, lightgray, fill, fill opacity=0.1] (0,0) -- (56.7:0.4) arc (56.7:90.:0.4) -- cycle;
      \draw [shift={(0,0)}, lightgray, fill, fill opacity=0.1] (0,0) -- (-135.7:0.4) arc (-135.7:-33.2:0.4) -- cycle;
      \draw [color=draculared] (0,0) circle (2cm);
      \draw [rotate around={0.:(0.,0.)},dash pattern=on 3pt off 3pt,color=draculared] (0,0) ellipse (2cm and 0.9cm);
      \draw (0,0)-- (0.70,1.07);
      \draw [->] (0,0) -- (0,2);
      \draw [->] (0,0) -- (-0.81,-0.79);
      \draw [->] (0,0) -- (2,0);
      \draw [dotted] (0.7,1)-- (0.7,-0.46);
      \draw [dotted] (0,0)-- (0.7,-0.46);
      \draw (-0.08,-0.3) node[anchor=north west] {\pink{$\varphi$}};
      \draw (0.01,0.9) node[anchor=north west] {\pink{$\theta$}};
      \draw (-1.01,-0.72) node[anchor=north west] {};
      \draw (2.07,0.3) node[anchor=north west] {};
      \draw (-0.5,2.6) node[anchor=north west] {\comment{$|0\rangle$}};
      \draw (-0.4,-2) node[anchor=north west] {\comment{$|1\rangle$}};
      \draw (0.4,1.65) node[anchor=north west] {$|\green{\psi}\rangle$};
      \scriptsize
      \draw [fill] (0,0) circle (1.5pt);
      \draw [fill] (0.7,1.1) circle (0.5pt);
    \end{tikzpicture}}
}

%%Abstract:
%Popular descriptions of quantum computers can make the distinction between a quantum and
%an analog classical computer difficult to discern.  We will describe why these can be
%radically different concepts, in particular because the analogy between a qubit and a
%classical bit is not straightforward.  We will also show that the quantum circuit
%paradigm for quantum computing is an approximation and discuss the inevitable
%introduction of noise.  Lastly, we will briefly discuss the (likely more familiar)
%subject of why there has recently been a flurry of interest in the subject among
%surveillance capitalist enterprises, why that interest is likely to wane, and give a
%brief advertisement for the much underappreciated prospect of adiabatic (reversible)
%classical computing.  The talk will not assume any previous knowledge of quantum
%mechanics, but a grasp of basic linear algebra will be helpful.

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\begin{frame}
    \frametitle{table of contents}

    \begin{itemize}
        \item \purple{$e^{-iHt}$} What is \cyan{quantum mechanics}?
        \item \purple{$|01\rangle$} \cyan{Qubits} are not like \red{classical bits}
            \orange{\texttt{0b01}}.
        \item 🚪 Quantum gates, circuits, why these are \yellow{approximations}, and why all quantum
            computers are \pink{noisy}.
        \item 🔥 Why does (or did) silicon valley care about this \purple{arcane subject}?
        \item 🪐 Universal bounds on computation and an advertisement for \green{adiabatic
            (reversible) classical computers}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{QM in a picosecond}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \green{States} in a \purple{Hilbert space}:\\
            {\Large$$|\green{\psi}\rangle \in \purple{\mathcal{H}}$$}\\
            \bigskip
            You can think of this as a \yellow{``normal" vector space}, Hilbert just means it's
            🟣 ``complete" \comment{(e.g. $\mathbb{R}$ is a Hilbert space, $\mathbb{Q}$ is
            not)}

            \bigskip

            For any \green{\emph{physical} state}
            $$\langle\green{\psi}|\green{\psi}\rangle=\red{1} ~~~ \comment{\text{(unit
            norm)}}$$
            and \orange{overall phase is \emph{unobservable}} \comment{(i.e. $|\psi\rangle\to
            e^{i\alpha}|\psi\rangle$ is the same system)}.
        \end{column}
        \begin{column}{0.5\textwidth}
            {\large \cyan{Schrodinger Equation}:}

            {\Large $$|\green{\psi}(\pink{t})\rangle =
            e^{-i\orange{H}\pink{t}}|\green{\psi}(\comment{0})\rangle$$}

            \bigskip
            where $e^{-i\orange{H}\pink{t}}$ is a \yellow{\emph{unitary} operator}
            (\orange{$H$} is Hermitian)\footnote{You
                can think of this as a matrix $U$ the conjugate transpose of which is its
                inverse $U^{\dagger}U=1$.}\footnote{Don't confuse \orange{$H$} with
                \purple{$\mathcal{H}$}!  \orange{$H$} is just an operator (think matrix),
            \purple{$\mathcal{H}$} is the state space of our system.}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{simplest non-trivial example}

    🪨 Here's a \red{stationary system}:
    \begin{equation*}
        \underbrace{\left(\begin{array}{cc}
                    e^{-i\pink{\omega_0} t} & \comment{0} \\
                    \comment{0} & e^{-i\pink{\omega_1} t}
        \end{array}\right)}_{e^{-i\orange{H}t}}
        \underbrace{\left(\begin{array}{c}
                    1 \\ \comment{0}
        \end{array}\right)}_{|\green{\psi}(0)\rangle}
        =
        \underbrace{\left(\begin{array}{c}
                    e^{-i\pink{\omega_0} t} \\ \comment{0}
        \end{array}\right)}_{|\green{\psi}(t)\rangle}
        =
        \underbrace{\comment{e^{-i\omega_0 t}}}_{\yellow{\text{unphysical phase factor}}}
        \underbrace{\left(\begin{array}{c}
                    1 \\ \comment{0}
        \end{array}\right)}_{|\green{\psi}(0)\rangle}
    \end{equation*}

    🎡 Here's a \cyan{rotating system}:\footnote{\comment{I'm omitting normalization factors because they're
    annoying.}}

    \begin{equation*}
        \underbrace{\left(\begin{array}{cc}
                    e^{-i\pink{\omega_0} t} & \comment{0} \\
                    \comment{0} & e^{-i\pink{\omega_1} t}
        \end{array}\right)}_{e^{-i\orange{H}t}}
        \underbrace{\left(\begin{array}{c}
                    1 \\ 1
        \end{array}\right)}_{|\green{\psi}(0)\rangle}
        =
        \underbrace{\left(\begin{array}{c}
                    e^{-i\pink{\omega_0} t} \\ e^{-i\pink{\omega_1} t}
        \end{array}\right)}_{|\green{\psi}(t)\rangle}
        =
        \underbrace{\comment{e^{-i\omega_0 t}}}_{\yellow{\text{unphysical phase factor}}}
        \underbrace{\left(\begin{array}{c}
                    1 \\ e^{-i(\pink{\omega_1} - \pink{\omega_0})t}
        \end{array}\right)}_{\text{new state!}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{1 qubit}

    \begin{center}
        \blochsphere{0.6}
        $$|\green{\psi}\rangle = \cos(\pink{\theta}/2)|0\rangle +
        e^{i\pink{\varphi}}\sin(\pink{\theta}/2)|1\rangle$$
    \end{center}

    \begin{itemize}
        \item state of \cyan{simplest non-trivial quantum system}, we'll call these
            \purple{$\mathcal{H}_1$}.
        \item 2 basis states ($|\comment{0}\rangle$ and $|\comment{1}\rangle$) over $\mathbb{C}$
        \item Unit norm ($\langle\green{\psi}|\green{\psi}\rangle=\red{1}$)
        \item Same physical state under phase shift ($|\green{\psi}\rangle\to
            e^{i\alpha}|\green{\psi}\rangle$).
    \end{itemize}

    This is a state with \yellow{two continuous, compact parameters} with the \red{topology of a
    sphere}.
\end{frame}

\begin{frame}
    \frametitle{multiple qubits: Hilbert space outer product}
    
    \purple{$\mathcal{H}_1$} is \yellow{two dimensional} with basis states $|\comment{0}\rangle$ and
    $|\comment{1}\rangle$.\\
    \bigskip
    To describe \red{multiple qubit states}, we take the \purple{Hilbert space}
    \orange{outer product}
    $$\purple{\mathcal{H}_n} = \underbrace{\purple{\mathcal{H}_1} \otimes
        \purple{\mathcal{H}_1} \otimes \cdots \otimes
    \purple{\mathcal{H}_1}}_{n ~\text{times}} $$

    The basis of \purple{$\mathcal{H}_n$} consists of the outer products of the bases of the
    \purple{$\mathcal{H}_1$}.\\
    \bigskip
    For example, using the shorthand notation \pink{$|\psi\phi\rangle =
    |\psi\rangle\otimes|\phi\rangle$}, a
    basis of \purple{$\mathcal{H}_2$} is
    $$\Bigl\{\; |\comment{00}\rangle,\; |\comment{01}\rangle,\; |\comment{10}\rangle,\;
    |\comment{11}\rangle\;\Bigr\}$$

    We refer to this as the \green{\emph{computational basis}}.
\end{frame}

\begin{frame}
    \frametitle{2 qubits: first attempt}
    \centering

    \includegraphics[scale=0.04]{thales1}\\
    \orange{(classical)} \yellow{\emph{``It's obviously just the outer product of two spheres."}}\\
    \begin{tikzpicture}
        \node (a) at (-1, 0) {\blochsphere{0.4}};
        \node (inner) at (0,0) {$\otimes$};
        \node (b) at (1.2, 0) {\blochsphere{0.4}};
    \end{tikzpicture}

    That would mean \green{\textbf{4 dimensions}} in the topology of a pair of spheres.

    Note that this makes perfect sense for an analog classical computer with sphere
    registers, which we'll dub ``🔴 \red{spherebits}".

    {\tiny \comment{This approach is arguably implied by lots of popular science and news
    articles.}}
\end{frame}

\begin{frame}
    \frametitle{digression: counting parameters}

    \centering

    Let's count how many \yellow{real parameters} we have for \purple{$n$} binary qubits.

    $$\underbrace{\green{2}}_{\text{complex}}\cdot\underbrace{\orange{2}^\purple{n}}_{\text{this many dimensions in
    }\purple{\mathcal{H}}} - \underbrace{\red{1}}_{\langle\green{\psi}|\green{\psi}\rangle=1} -
    \underbrace{\red{1}}_{\text{overall phase}}$$

    $$\green{2}(\orange{2}^\purple{n}-\red{1})$$

    \begin{itemize}
        \item For $\purple{n}=\pink{1}$ we get $\orange{2}$, as expected from the Bloch sphere.
        \item For $\purple{n}=\pink{2}$ we get \red{$\mathbf{6}$}!  That's \yellow{2 more
            continuous parameters} that we got from $🔴\otimes 🔴$ combining spherebits!
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{where are the extra parameters coming from?}

    \centering

    The \orange{product of two spherebits} $🔴\otimes 🔴$ corresponds to states of the form
    $$\bigl(\pink{\alpha_0} |\comment{0}\rangle + \pink{\alpha_1}
        |\comment{1}\rangle\bigr)\otimes\bigl(\pink{\beta_0}|\comment{0}\rangle +
    \pink{\beta_1}|\comment{1}\rangle\bigr)$$
    but this \yellow{\emph{does not describe all the states}} in $\purple{\mathcal{H}_1}
    \otimes\purple{\mathcal{H}_1}$\\

    consider, for example, the state
    $$\frac{1}{\sqrt{2}}\bigl(|\comment{00}\rangle +
    |\comment{11}\rangle\bigr)$$

    This \red{\emph{cannot be written}} as a product $|\green{\psi}\rangle\otimes|\green{\phi}\rangle$ where
    $|\green{\psi}\rangle$ is a state of the first qubit and $|\green{\phi}\rangle$ is a state of the
    second!

    \bigskip

    This as an \cyan{\emph{entangled} state}, to which there is \orange{no equivalent in the
    classical limit}!
\end{frame}

\begin{frame}
    \frametitle{lesson}

    \begin{itemize}
        \item \cyan{Qubits} are \yellow{\emph{not analogous}} to \orange{classical bits} (or analog registers).
            \begin{itemize}
                \item \orange{Classical bits} combine via an \pink{outer product
                    $\otimes$}.
                \item The state of a \cyan{quantum register} is an $2(2^\purple{n}-1)$ sphere
                    🟣.
            \end{itemize}
        \item Distinction between analog and digital in quantum case is less clear, but 
            computer with finitely many qubits is \purple{more like a digital computer}.
        \item \cyan{Quantum states} are \pink{{\Large``huge"}}.  A 64-bit number has
            \red{$2^{64}$} possible
            \yellow{\emph{values}}, a 64-qubit register has a state with
            \red{$\approx2^{65}$} \yellow{\emph{continuous
            parameters}}!\footnote{\comment{The
                    matter of how many classical bits can be ``stored in" a qubit is more
                    complicated, and not as many as you might think, but nevertheless the huge
            number of dimensions of the state space is very real.}}
        \item What constitutes a qubit is \yellow{\emph{basis dependent}}.  In principle there is
            \green{no distinction between a register of $2$ binary qubits or $1$ ququad}.
            \footnote{\comment{Implementations usually encode qubits on ``physically separate"
                degrees of freedom, but we'd need to introduce more QM to understand what
            that really means.}}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{local operators}
    
    The concept of \yellow{\emph{locality}} is central to quantum theories that describe the real
    world.\\
    \bigskip
    Often, when we \pink{factor} a state like
    $\ket{\green{\Psi}}=\ket{\purple{\psi_1}}\ket{\cyan{\psi_2}}$ we imply that
    $\ket{\purple{\psi_1}}$ and $\ket{\cyan{\psi_2}}$ are \orange{``physically separated"} in some
    sense.\footnote{\comment{I'm going to punt on what this really means for the purpose of this
    talk, but assume we mean spatial separation for now.}}\\
    \bigskip
    We call an operator of the form
    \begin{equation*}
        \purple{A}\otimes\cyan{\mathbf{1}} ~~~~~~ \text{or} ~~~~~~
        \purple{\mathbf{1}}\otimes\cyan{A} 
    \end{equation*}
    where the first factor acts on $\ket{\purple{\psi_1}}$ and the second factor acts on
    $\ket{\cyan{\psi_2}}$ \yellow{\emph{local}}.\\
    If $\ket{\purple{\psi_1}}$ and $\ket{\cyan{\psi_2}}$ are far apart, it's probably true
    that only \yellow{local operators} are applied to them!\\
    \bigskip
    These operators \red{\textbf{cannot entangle} (or disentangle)}.\footnote{\comment{Again, this
            is \yellow{basis dependent}, so we mean they can't entangle in the basis with respect to
            which they are local.}}
\end{frame}

\begin{frame}
    \frametitle{quantum circuits}
    We have seen that quantum mechanics works by \cyan{\emph{evolution by a unitary
    operator}}.\\
    So, we can build a \green{computer} by chaining together operations on a register, as long as
    they are \orange{\textbf{all unitary}}.\\
    This is called a \pink{\emph{quantum circuit}}.  Usually what is meant by
    💻 ``\red{quantum computer}"
    is a bunch of these circuits.
    
    \begin{figure}
            \begin{quantikz}[transparent, color=draculafg]
                \lstick{\ket{\green{k_1}}} & \gate{\purple{H}} & \gate{\red{R_2}} & \push{} &
                \gate[2,swap]{} & \push{} & \rstick{$\ket{\comment{0}}+e^{i\pi
                \green{0.k_2}}\ket{\comment{1}}$}\\
                \lstick{\ket{\green{k_2}}} & \push{} & \ctrl{-1} & \gate{\purple{H}} & &
                \push{} & \rstick{$\ket{\comment{0}}+e^{i\pi
                \green{0.k_1 k_2}}\ket{\comment{1}}$}
            \end{quantikz}
            \caption{Circuit for quantum fourier transform. \comment{$0.k_1\cdots k_n$ is
            notation for $k_1 2^{-1} + \cdots +k_n 2^{-n}$.}}
    \end{figure}

    Operators appearing on a single line are \yellow{local}! This drawing is just a 🍾 \orange{fancy notation} for
    $$S(\comment{\mathbf{1}}\otimes
    \purple{H})\red{R_2}(\purple{H}\otimes \comment{\mathbf{1}})|\green{k_1 k_2}\rangle$$
    \footnote{\comment{In case you're keeping count, this is the 3rd time we've used some
    variation of the symbol $H$ to mean something completely different.}}
\end{frame}

\begin{frame}
    \frametitle{wait a minute!}
    
    We just said how quantum mechanics works, and that \red{isn't it}.\\
    \bigskip
    \cyan{Schrodinger equation} says $e^{-i\orange{H}\pink{t}}|\green{\psi}\rangle$; it doesn't say
    you get to swap out a bunch of different operators!\\
    \bigskip
    The quantum circuits we've written down are instead
    \begin{equation*}
        \theta(\pink{t} - t_0)\,\theta(t_1 - \pink{t})\,e^{-i\orange{H_1}\pink{t}} +
        \theta(\pink{t} - t_1)\,\theta(t_2 - \pink{t})\, e^{-i\orange{H_2}\pink{t}} + \cdots
    \end{equation*}

    Getting this out of $e^{-i\orange{H}\pink{t}}$ is \purple{highly non-trivial} and necessarily
    involves much \yellow{larger quantum systems}.\\
    \bigskip
    Even from a very broad theoretical view we see that all \pink{quantum circuits} are
    approximations and will get us embroiled in \orange{lots of implementation details}!
\end{frame}

\begin{frame}
    \frametitle{error correction}
    
    What can we do to \orange{mitigate errors}?\\
    \bigskip
    It works about like you'd expect, rather than encoding into a single qubit, we encode
    into multiple, for example:
    $$\pink{\alpha}\ket{\cyan{0}} + \pink{\beta}\ket{\cyan{1}} ~~~\to~~~
    \pink{\alpha}\ket{\cyan{0}\comment{00}} + \pink{\beta}\ket{\cyan{1}\comment{11}}$$

    However, there are some ⚠ \red{major challenges} relative to the classical case:
    \begin{itemize}
        \item We need at least \textbf{\orange{5}} qubits to be able to detect arbitrary
            \textbf{\orange{1}} qubit errors.
        \item The \yellow{\emph{no cloning theorem}} says we can't just copy an arbitrary state.
            \comment{(This  is obvious if you think about how unitary operations work!)}
        \item As we previously alluded, quantum states are \pink{{\large ``huge"}} and live in a
            fundamentally \yellow{\emph{continuous}} parameter space!\footnote{\comment{The notion that QM
            is all about discrete objects is a malicious lie 🤬.}}
        \item I've so far studiously avoided the topic of \purple{measurements}, but suffice it to
            say they \red{do not, in general, preserve states}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{period finding}

    \begin{itemize}
        \item \pink{What useful things can a quantum computer do?}
            \begin{itemize}
                \item This is a rather \orange{difficult question to answer}.  As we hope
                    has become apparent, \cyan{coming
                    up with quantum algorithms is hard}, and involves a \red{radically different intuition from
                    classical computing}.
            \end{itemize}
    \end{itemize}
    
    \bigskip

    🔥 A ``\red{killer app}" for quantum computers is \purple{finding the period of
    functions} \comment{(more specifically, finding a ``hidden" subgroup)}.  Naturally,
    this is given to us by the 🌊 \cyan{quantum fourier transform}.
    \begin{equation*}
        |\green{j}\rangle ~~\longrightarrow~~ \frac{1}{\sqrt{N}}\sum_{k=0}^{N-1}e^{2\pi
        i\green{jk}/N}|\green{k}\rangle
    \end{equation*}

    This operation is \yellow{\emph{unitary}}, so we can build a \purple{quantum circuit} for it!\\

    \bigskip

    \green{Finding the period of a function} doesn't sound like a 🤪 \red{cockamamie evil scheme} to take
    over the world, so why would \comment{google, microsoft, amazon et al.} be interested in
    this?\footnote{\textbf{\orange{spoiler alert:}} \comment{it's a cockamamie evil scheme to
    take over the world}}
\end{frame}

\begin{frame}
    \frametitle{discrete logarithms}
    
    Thanks to some confusing facts about discrete 🔢 \red{number theory}\footnote{\comment{Basically,
    the periodicity of $y^a\mod N$ for $a \in \mathbb{Z}_{\ge}$.}}, the hardest part of
    \yellow{\textbf{discrete logarithms}} \comment{(e.g. for factorizing integers)} essentially involves
    \cyan{finding a period}.\\

    \bigskip

    As we well know, the 🔒 \orange{security} of the most commonly deployed \orange{crypto-systems, DH, RSA and
    ECC} \comment{(and variants thereof)} is derived from the \yellow{difficulty of
    discrete logarithms.}\\

    \bigskip
    The factorization algorithm itself is the famous \green{Shor's algorithm}.  It is
    😖 not simple
    to contemplate, but it basically amounts to \purple{applying the quantum fourier
    transform} for
    period finding into a standard \red{factorization} procedure.

    For an \purple{$n$}-bit number, the speedup is:
    \begin{equation*}
        O\Bigl(\pink{e}^{\purple{n}^{1/3}\log^{2/3}(\purple{n})}\Bigr) ~~\longrightarrow~~
        O(\purple{n}^2)
    \end{equation*}
    \yellow{(which is obviously very dramatic)}
\end{frame}

\begin{frame}
    \frametitle{lattice cryptography}
    
    That humans use a set of 🔒 \orange{cryptography algorithms} for which there is an
    \purple{exponential
    quantum speedup} seems to be somewhat of a \red{historical accident}.\\

    \bigskip
    Here's another class of problems for which there is \green{no known quantum speedup}, but
    which can be used for 🔒 \orange{encryption}.  Consider the 💠 lattice
    \begin{equation*}
        \green{\mathfrak{L}} = \Bigl\{\sum \pink{a_i} \purple{\vec{b}_i} \mid \pink{a_i} \in \mathbb{Z}\Bigr\} ~~~~~~~~
        \comment{\text{where}}\; \purple{\vec{b}}\in\mathbb{R}^n
    \end{equation*}
    The problem of finding $\purple{\vec{v}}\in\green{\mathfrak{L}}$ that minimizes
    $|\purple{\vec{v}}| > 0$ has \yellow{no
    known solutions (quantum or classical) with sub-exponential time.}\\
    \bigskip

    There are also other classes of problems for which no quantum speedups are known.
    \pink{NIST} already working on standardizing \orange{post-quantum crypto}.\\

    {\tiny See \cyan{\underline{\texttt{https://en.wikipedia.org/wiki/NIST\_Post-Quantum\_Cryptography\_Standardization}}}
    for a list of algorithms.}

    {\tiny Also in the news: Signal adopts \texttt{CRYSTALS-Kyber}
    \cyan{\underline{\texttt{https://signal.org/blog/pqxdh/}}}.}
\end{frame}

\begin{frame}
    \frametitle{simulating other quantum systems}
    
    What might someone who is \comment{\emph{not} 🤪 trying to take over the world} do
    with a \cyan{quantum computer?}\\
    \bigskip
    An obvious use case is \purple{simulating other} \comment{(presumably harder to
    access)} \cyan{quantum
    systems}.  We decompose the Hamiltonian $\orange{H}=\sum_{k} \orange{H_k}$.  In realistic systems the
    \orange{$H_k$} are local in some sense.  We can use
    \begin{equation*}
        e^{i(\red{A} + \red{B})\,\pink{\delta t}} = e^{i\red{A}\,\pink{\delta
        t}}e^{i\red{B}\, \pink{\delta t}} + O(\pink{\delta t}^2)
    \end{equation*}
    This suggests we build a 💻 \cyan{quantum computer} with a series of gates
    $e^{-i\orange{H_k}\,\pink{\delta
    t}}$.\\

    \bigskip

    There are also some \green{weird non-local Hamiltonians which can be efficiently
    simulated} \yellow{(maybe this hints at simulation for strongly interacting
    systems?)}\footnote{\comment{My references, in particular Nielsen and Chuang, don't
            really make this leap, they just say these are weird Hamiltonians, though I've
    definitely heard people other than me suggest things like this.  Would love to hear a
    more detailed perspective from experts on the subject.}}\\
    \bigskip
    On the other hand, there are still ⚠ \red{major challenges}.  For one, there are Hamiltonians
    that are \yellow{exponentially hard to approximate using finitely many gate types}.
\end{frame}

\begin{frame}
    \frametitle{this is not the only way forward}
    
    \cyan{Quanum computing} is most definitely \red{\emph{not}} the only conceivable way to increase
    💻 \orange{computing capability}.\\
    \smallskip
    Let's get some 👀 \green{perspective}.
    \begin{itemize}
        \item \red{Bremermann's Limit}: Lowest known upper bound on \orange{rate of
            computation} $$(2\pi)^{-1}
            \sim 10^{50}~\text{s}^{-1}\text{kg}^{-1}$$
            Best computers today: $\sim 10^{16}~\text{s}^{-1}\text{kg}^{-1}$
        \item \purple{Bekenstein Bound}: Lowest known upper limit on \orange{information
            density}
            $$(4\log(2)~l_{P}^2)^{-1}\sim 10^{69}~\text{m}^{-2}$$
            By comparison $1\,\text{TB}\,\text{cm}^{-2} = 8\cdot10^{16}\,\text{m}^{-2}$.
    \end{itemize}

    \green{\textbf{Conclusion:}} We are still 😆 laughably \yellow{\emph{far away}} from
    the \red{known limits of
    computation}, and \yellow{it's not clear any of this requires quantum
    computers}.\footnote{\comment{There's a lot of subtlety to both of these bounds, but more
    careful examination doesn't change the broad conclusion.}}
\end{frame}

\begin{frame}
    \frametitle{example alternative paradigm: reversible classical computing}
    
    \cyan{Quantum computing} evades bounds on energy consumption by being fundamentally
    \yellow{\emph{reversible}}, but are \green{hardly a necessary condition for this}.\\
    \smallskip
    A \orange{classical computing} paradigm which is somewhat more like quantum computer than
    today's von Neumann machines are \green{\emph{classical adiabatic computers}}, which operate
    using \purple{circuits analogous to quantum circuits}.\\
    \smallskip
    \pink{Living in a world with only CMOS von Neumann computers is more a consequence of
    history than physics!}\\
    \smallskip
    Some wiki pages:
    \begin{itemize}
        \item On \green{adiabatic computing}:
            \cyan{\underline{\texttt{https://en.wikipedia.org/wiki/Reversible\_computing}}}
        \item ``\yellow{Unconventional computing}":
            \cyan{\underline{\texttt{https://en.wikipedia.org/wiki/Unconventional\_computing}}}
        \item 🔍 \purple{Optical computing}:
            \cyan{\underline{\texttt{https://en.wikipedia.org/wiki/Optical\_computing}}}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{end}
    \centering

    \scalebox{2}{🛑}

    \cyan{thanks}
\end{frame}

\appendix

\begin{frame}
    \frametitle{(appendix) measurement}

    We start with state $\ket{\green{\psi}}=\pink{\alpha}\ket{\green{0}}+\pink{\beta}\ket{\green{1}}$.\\
    \smallskip
    There is a \orange{detector} with state $\ket{\orange{D}}$ and an \red{environment} with state
    $\ket{\red{E}}$.
    These are \textbf{\large \red{macroscopic}}, with effectively infinite dimensional state
    spaces.\\
    \smallskip
    At time \yellow{$t_1$} and \yellow{$t_2$}, respectively, the detector and environment become entangled
    with $\ket{\green{\psi}}$.
    
    \begin{align*}
        \text{at}\; \yellow{t_0} & : &
        \bigl(\pink{\alpha}\ket{\green{0}}+\pink{\beta}\ket{\green{1}}\bigr)\ket{\orange{D}}\ket{\red{E}}\\
        \text{at}\; \yellow{t_1} & : & \bigl(\pink{\alpha}\ket{\green{0}}\ket{\orange{D_0}} +
        \pink{\beta}\ket{\green{1}}\ket{\orange{D_1}}\bigr)\ket{\red{E}}\\
            \text{at}\; \yellow{t_2} & : &
            \pink{\alpha}\ket{\green{0}}\ket{\orange{D_0}}\ket{\red{E_0}} +
            \pink{\beta}\ket{\green{1}}\ket{\orange{D_1}}\ket{\red{E_1}}
    \end{align*}

    Because the state space is so large
    $\braket{\orange{D_0}}{\orange{D_1}}\approx\braket{\red{E_0}}{\red{E_1}}\approx
    \comment{0}$.\\
    \smallskip
    Furthermore, $\ket{\green{0}}\ket{\orange{D_0}}\ket{\red{E_0}}$ and
    $\ket{\green{1}}\ket{\orange{D_1}}\ket{\red{E_1}}$ will
    \yellow{\emph{never interfere again}}, this is called \cyan{\textbf{decoherence}}.  We now refer to
    these as separate 🌳 \purple{\emph{branches of the wavefunction}}.\\
    \smallskip
    What is the state of \orange{$D$} at \yellow{$t_2$}?  As far as we have discussed, this is not even a
    valid question because \orange{$D$} is maximally entangled.\\
    \smallskip
    We now say that \orange{$D$} is in $\ket{\orange{D_0}}$ with probability
    $|\pink{\alpha}|^2$ and $\ket{\orange{D_1}}$
    with probability $|\pink{\beta}|^2$.  This is the \yellow{\emph{unique}} self-consistent way to talk
    about \orange{$D$} in the entangled state.
\end{frame}

\begin{frame}
    \frametitle{superdense coding}
    
    We only briefly mentioned how many \red{classical bits} can be stored per
    \purple{qubit}.  In this
    \yellow{protocol}, a \purple{single qubit} can be used to send two \red{classical
    bits}.\\
    \smallskip
    👩 \pink{Deandra} and 👨 \green{Charlie} each get a qubit, from the state
    \begin{equation*}
        2^{-1/2}\bigl(\ket{\pink{0}}_\pink{D}\ket{\green{0}}_\green{C}+
        \ket{\pink{1}}_\pink{D}\ket{\green{1}}_\green{C}\bigr)
    \end{equation*}

    👩 Sweet \pink{Dee} wants to send 👨 \green{Charlie} a 2-bit 📧 message.  She transforms her qubit according to the
    following dictionary:
    \begin{align*}
        \red{\texttt{0b00}} & : & \ket{\pink{0}\green{0}}+\ket{\pink{1}\green{1}}\\
        \red{\texttt{0b01}} & : & \ket{\pink{0}\green{0}}-\ket{\pink{1}\green{1}}\\
        \red{\texttt{0b10}} & : & \ket{\pink{1}\green{0}} + \ket{\pink{0}\green{1}}\\
        \red{\texttt{0b11}} & : & -\ket{\pink{1}\green{0}} + \ket{\pink{0}\green{1}}
    \end{align*}

    These states are \yellow{mutually orthogonal}, so when 👩 Sweet \pink{Dee} sends 👨
    \green{Charlie} her qubit, he can
    simply read off the 📧 message!\\
    \smallskip
    \purple{This protocol involved 2 qubits, but only required sending 1.}
\end{frame}


\end{document}
