using Term, XGBoost, LinearAlgebra, RDatasets
df = dataset("vcd", "VonBort")[:, [:Deaths, :Year, :Corps]];
df[!, :Corps] = map(x -> Int(x.ref), df.Corps);
(X, y) = (select(df, :Year, :Corps), df.Deaths);
b = xgboost((X, y), num_round=12)
ŷ = predict(b, X);
rmse = √(norm(ŷ - y))
importancereport(b)
