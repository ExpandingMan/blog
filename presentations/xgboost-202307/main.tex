\documentclass[10pt,aspectratio=169]{beamer}

\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hwemoji}

\usepackage{tikz}
\usetikzlibrary{arrows,shapes,positioning}

\graphicspath{ {./images/} }

%https://github.com/walmes/Tikz for useful tikz examples

\tikzstyle{tbox} = [    
rectangle,
rounded corners,
draw = draculabg,
fill = draculacomment,
text width = 8em,
text centered,
]
\tikzstyle{line} = [draw=draculafg, -latex']


\usetheme{Boadilla}
\usecolortheme{dracula}
\usefonttheme{serif}

\title{XGBoost.jl}
\author{Mike Savastio (Expanding Man)}
\date{}

\newcommand{\todo}[1]{{\color{draculared}TODO: #1}}
\newcommand{\ttpink}[1]{{\color{draculapink}\texttt{#1}}}
\newcommand{\ttcyan}[1]{{\color{draculacyan}\texttt{#1}}}
\newcommand{\ttcomment}[1]{{\color{draculacomment}\texttt{#1}}}
\newcommand{\ityellow}[1]{{\color{draculayellow}\emph{#1}}}
\newcommand{\itred}[1]{{\color{draculared}\emph{#1}}}

\newcommand{\cyan}[1]{{\color{draculacyan}#1}}
\newcommand{\green}[1]{{\color{draculagreen}#1}}
\newcommand{\orange}[1]{{\color{draculaorange}#1}}
\newcommand{\pink}[1]{{\color{draculapink}#1}}
\newcommand{\purple}[1]{{\color{draculapurple}#1}}
\newcommand{\red}[1]{{\color{draculared}#1}}
\newcommand{\yellow}[1]{{\color{draculayellow}#1}}
\newcommand{\comment}[1]{{\color{draculacomment}#1}}


\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\begin{frame}
    \frametitle{🌳 what is xgboost?}
    
    \begin{center}
        \includegraphics[scale=0.6]{logo}

        \textbf{\yellow{eXtreme Gradient Boosting}}
    \end{center}

    \begin{itemize}
        \item \pink{C++} library for gradient boosted trees
        \item bindings in tons of languages, mostly via \pink{C} library
        \item one of the most popular machine learning
            packages\textsuperscript{\comment{[citation needed]}}
    \end{itemize}

    \bigskip

    \orange{\textbf{what the hell is gradient boosting?}}
    successive approximation by adding a new model (here a \green{tree}) on each iteration

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{align*}
                \purple{\hat{y}_1} &= \red{f_1}(x)\\
                \purple{\hat{y}_2} &= \purple{\hat{y}_1} + \red{f_2}(x) \\
                          & \cdots \\
                \purple{\hat{y}_n} &= \sum_{k=1}^{n}\red{f_k}(x)
            \end{align*}
        \end{column}
        \begin{column}{0.5\textwidth}
            objective at $k$th iteration
            $$\sum_{j\in\text{datapoints}}\bigl[\cyan{g_j} \red{f_k}(x_j) + \frac{1}{2}\cyan{h_j}
            \red{f_{k}}^{2}(x_j)\bigr]+\omega(\red{f_k})$$
            where $\cyan{g_j}$ and $\cyan{h_j}$ are 1st and 2nd order derivatives of loss $\ell$
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{💻 important recent contributors}
    \begin{itemize}
        \item Tyler Thomas (\ttcyan{@tylerjthomas9})
        \item Avik Sengupta (\ttcyan{@aviks})
        \item David Widmann (\ttcyan{@devmotion})
        \item Jiaming Yuan (\ttcyan{@trivialfis})
        \item Anthony Blaom (\ttcyan{@ablaom})
        \item Mike Savastio (\ttcyan{@ExpandingMan}) and 🐈 Calypso
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{\yellow{XGBoost.jl} TL;DR}
    
    \centering
    \includegraphics[scale=0.35]{feature-demo-1}
\end{frame}

\begin{frame}
    \frametitle{\yellow{XGBoost.jl} features}
    
    \begin{itemize}
        \item automated wrapping of \ttpink{libxgboost} via \yellow{Clang.jl}
        \item \yellow{Tables.jl} compatibility (for data input)
        \item parameter tracking (not supported by \pink{C} lib)
        \item feature name tracking (via tables)
        \item \green{CUDA} support, via extension (thanks to \ttcyan{@tylerjthomas9} for
            dealing with BinaryBuilder stuff!).  Automatic via \texttt{\green{CuArray}}.
        \item Pretty terminal displays via \yellow{Term.jl}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{\yellow{XGBoost.jl} features (continued)}
    
    \begin{itemize}
        \item Custom \orange{loss functions} (requires $\partial$ and $\partial^2$)
        \item \green{CUDA} and \yellow{Term.jl} are lazily loaded via extensions on 1.9
            (thanks to \ttcyan{@devmotion})
        \item \purple{Feature importance} reporting, as table or \yellow{Term.jl} display
        \item Full introspection of model as \yellow{AbstractTrees.jl} compatible tree
            objects (\ttpink{trees(::Booster)}).
        \item Default parameter sets for \ttpink{regression}, \ttpink{countregression},
            \ttpink{classification}, \ttpink{randomforest}.
        \item \yellow{MLJ.jl} compatibility via \yellow{MLJXGBoostInterface.jl}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{🪟 the infamous windows error (\cyan{\#153}, \cyan{\#179})}

    Starting with \purple{Julia 1.8.4}, attempting to run any function from
    \ttpink{libxgboost} would cause \red{a spectacular failure with \emph{no terminal
    output}} on 🪟 \orange{windows}.

    \bigskip

    Turned out to be a 🐞 \red{bug} in \yellow{GCC} \ttpink{libgomp} that required a patch in
    one of \purple{Julia}'s compiler support libraries.

    \bigskip

    {🔧} \green{Fixed} as of \purple{Julia 1.9.2} (\cyan{\#50135}).  Thanks to James
    Foster (\ttcyan{@jd-foster}), Mark Kittisopikul (\ttcyan{@mkitti}) and Mose Giordano (\ttcyan{@giordano})!
\end{frame}

\begin{frame}
    \frametitle{✅ TODO (wanted, future features)}
    
    \begin{itemize}
        \item Automatically differentiate custom loss functions with \yellow{Zygote}
        \item automatic handling of \yellow{CategoricalArrays}
        \item eliminate other \green{CUDA} related dependencies from \yellow{JLL}'s when not explicitly
            included (is this even possible on 1.9? re \ttcyan{@devmotion})
        \item \green{GPU CI/CD testing} with \orange{buildkite}
        \item \green{multi-GPU} support (\ttcyan{@tylerjthomas9})
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{pure Julia alternative: \yellow{EvoTrees.jl}\footnote{I have nothing to do
    with this package, just an interested observer 👀}}
    
    \begin{itemize}
        \item boasts extremely impressive benchmarks, especially on CPU
        \item many major features of \yellow{XGBoost} including:
            \begin{itemize}
                \item \green{GPU} (via \green{CUDA}) support
                \item \yellow{Tables.jl} compaitibility
                \item \orange{Feature importance} reporting
                \item Many tuning hyperparameters are available (e.g. \ttpink{lambda},
                    \ttpink{gamma}, \ttpink{eta}, \ttpink{max\_depth}, \ttpink{min\_weight})
            \end{itemize}
        \item \purple{\tiny(speculative)} Interesting possibilities from opening up the ``\comment{black box}"? (i.e.
            what interesting things can you do with \yellow{EvoTrees} that you can't do
            with \yellow{XGBoost} because it's in Julia?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{end}
    \centering
    
    \scalebox{2}{🛑}

    \cyan{thanks}
\end{frame}


\end{document}
