\documentclass[10pt,aspectratio=169]{beamer}

\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hwemoji}

\usepackage{tikz}
\usetikzlibrary{arrows,shapes,positioning}

\graphicspath{ {./images/} }

%https://github.com/walmes/Tikz for useful tikz examples

\tikzstyle{tbox} = [    
rectangle,
rounded corners,
draw = draculabg,
fill = draculacomment,
text width = 8em,
text centered,
]
\tikzstyle{line} = [draw=draculafg, -latex']


\usetheme{Boadilla}
\usecolortheme{dracula}
\usefonttheme{serif}

\title{Parquet in Julia}
\author{Mike Savastio (Expanding Man)}
\date{}

\newcommand{\todo}[1]{{\color{draculared}TODO: #1}}
\newcommand{\ttpink}[1]{{\color{draculapink}\texttt{#1}}}
\newcommand{\ttcyan}[1]{{\color{draculacyan}\texttt{#1}}}
\newcommand{\ttcomment}[1]{{\color{draculacomment}\texttt{#1}}}
\newcommand{\ityellow}[1]{{\color{draculayellow}\emph{#1}}}
\newcommand{\ttyellow}[1]{{\color{draculayellow}\texttt{#1}}}
\newcommand{\itred}[1]{{\color{draculared}\emph{#1}}}

\newcommand{\cyan}[1]{{\color{draculacyan}#1}}
\newcommand{\green}[1]{{\color{draculagreen}#1}}
\newcommand{\orange}[1]{{\color{draculaorange}#1}}
\newcommand{\pink}[1]{{\color{draculapink}#1}}
\newcommand{\purple}[1]{{\color{draculapurple}#1}}
\newcommand{\red}[1]{{\color{draculared}#1}}
\newcommand{\yellow}[1]{{\color{draculayellow}#1}}
\newcommand{\comment}[1]{{\color{draculacomment}#1}}


\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\begin{frame}
    \frametitle{💾 what is parquet?}
    \begin{itemize}
        \item \orange{columnar} binary data storage format
        \item Emerged from Twitter and Cloudera in 2013, \green{Apache Software
            Foundation} sponsored
        \item Widely successful binary format that has finally supplanted CSV at many
            institutions \textsuperscript{\comment{[citation needed]}}
        \item Data format based on 🪛 \yellow{``Dremel"} algorithm for flattening, assembling
            sparse hierarchical data structures
        \item Extremely ubiquitous in \cyan{``big data"} world particularly because of heavy use
            by \red{Apache Spark}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{brief history of parquet in julia}
    \begin{itemize}
        \item \yellow{Parquet.jl}: Tanmay Mohapatra (\ttcyan{@tanmaykm}) appeared in 2016
            \begin{itemize}
                \item First \pink{Thrift} (used for parquet metadata) implementation also by \ttcyan{@tanmaykm}
                \item \purple{write capability} in 2020
            \end{itemize}
        \item \yellow{Parquet2.jl}: Mike Savastio (\ttcyan{@ExpandingMan}) appeared in 2021
            \begin{itemize}
                \item Most work since has been maintenance and ensuring compatibility.
                \item \yellow{Thrift2.jl} in 2023, orders of magnitude faster IO with
                    significantly less GC overhead compared to \yellow{Thrift.jl}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{some code (\yellow{Parquet2.jl})}
    \centering
    \includegraphics[scale=0.33]{example1}
\end{frame}

\begin{frame}
    \frametitle{\yellow{Parquet2.jl} feature summary}
    \begin{itemize}
        \item Automatic handling of \red{\texttt{missing}} as null
        \item Full \yellow{Tables.jl} compatibility: both \ttpink{Dataset} (file or set of
            files) and \ttpink{RowGroup} (file subset) are \yellow{Tables.jl} tables.
        \item ``\purple{Lazy}" loading (metadata first, can load only selected columns and row
            groups.
        \item Supports all non-hierarchical data types including weird legacy 96-bit
            timestamps that the JVM loves.
        \item Support for 📁 \orange{multi-file} datasets.
        \item Support for \ttpink{snappy}, \ttpink{gzip}, \ttpink{zstd} and \ttpink{lz4}
            \cyan{compression} codecs.
        \item \yellow{FilePathsBase.jl} integration to read from remote file systems (e.g.
            \ttpink{Dataset("s3://path/to/file")}).
    \end{itemize}
\end{frame}

\begin{frame}
\begin{columns}
    \frametitle{which format should I use?}
    \begin{column}{0.5\textwidth}
        \scalebox{0.66}{
        \begin{tikzpicture}[node distance = 12em, auto]
            \node[tbox,diamond] (a) {is your data explicitly tabular?};
            \node[tbox,below right of = a] (b) {HDF5, BSON, language
                serializer, other format};
            \node[tbox,diamond,above right of = a] (c) {is someone forcing you to use
                parquet?};
            \node[tbox,below right of = c] (d) {arrow};
            \node[tbox,above right of = c] (e) {parquet};

            \path[line] (a) -- node {no} (b);
            \path[line] (a) -- node {yes} (c);
            \path[line] (c) -- node {no} (d);
            \path[line] (c) -- node {yes} (e);
        \end{tikzpicture}
    }
    \end{column}

    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item Parquet, like arrow, despite supporting hierarchical columns (but not in
                \yellow{Parquet2.jl}) is very explicitly tabular.
            \item Arrow has a natural data format similar to native formats of most
                languages, designed for \green{efficient access}.  Parquet is specialized for
                handling \red{nulls} and will never be as CPU efficient.
            \item Unclear advantages for parquet on disk.
            \item Parquet is far more common, but arrow already has huge investment from
                \ttpink{pandas} and \ttpink{polars} ecosystems.
        \end{itemize}
    \end{column}
\end{columns}
\end{frame}

\begin{frame}
    \frametitle{ecosystem, challenges}
    
    \begin{itemize}
        \item Format is \emph{\red{high entropy}} (lots of equivalent ways of representing
            same data)
            \begin{itemize}
                \item 2 different kinds of type metadata, other \orange{schema degeneracy}
                \item 9 different \orange{binary encodings} (at least 1 is pretty weird)
                \item 8 \cyan{compression codecs}
                \item 4 \orange{page types}
                \item \orange{metadata version} 1 and 2, supporting both for read is
                    unavoidable
                \item A lot of this is indisputably necessary, but it's \red{a lot to go
                    wrong} no matter how you look at it
            \end{itemize}
        \item I consider the \ttpink{pyarrow} implementation definitive. (i.e.
            \yellow{Parquet2.jl} should always both write files readable by
            \ttpink{pyarrow} and be able to read \ttpink{pyarrow} output.)
        \item A lot (the majority?) of what's found ``in the wild" is from \red{Spark or
            other JVM}, but it's a big pain to test against.
        \item \green{Thank you} to everyone who has opened an issue!  Many edge cases were
            🔧 fixed thanks to users providing samples.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{\yellow{Parquet2.jl}: outstanding issues}

    \begin{itemize}
        \item Support for \cyan{hierarchical data types}\footnote{If you need this you
            are just screwed, there's no way around it.  Not so for the other issues.}:
            \begin{itemize}
                \item Requires full implementation of \yellow{Dremel} which is rather complicated
                    and probably not very useful outside parquet.
                \item I considered only supporting e.g. 1 layer deep, but it would likely
                    be about $\frac{1}{2}$ as much work for $\ll\frac{1}{2}$ the result.
            \end{itemize}
        \item Better support for 📁 \orange{multi-file datasets}:
            \begin{itemize}
                \item Despite being an extremely common use case and some support via
                    metadata, \itred{there doesn't seem to be any universal standard or official
                    spec} for how this works.
                \item Dealing with remote file systems is a big pain.
                    (\yellow{FilePathsBase} could use some changes for remote)
            \end{itemize}
        \item Better support of \purple{newest metadata}:
            \begin{itemize}
                \item I would have preferred to only support latest metadata
                    standard for parquet, but legacy files are hugely important.
                \item \yellow{Parquet2} writes using the older metadata format.
            \end{itemize}
        \item \green{Type-stable} loading
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{future directions and alternatives}
    \begin{itemize}
        \item There is now \ityellow{Apache Arrow/Parquet GLib} which wraps the \cyan{C++} apache
            arrow project.
            \begin{itemize}
                \item \green{Arrow} opens up the possibility of such a wrapper being very
                    efficient in Julia.
            \end{itemize}
            
        \item There would be many benefits for a really good \ttpink{polars} wrapper.
            \begin{itemize}
                \item This is a viable option again largely thanks to arrow.
                \item I experimented with this, but \red{writing a rust FFI is really hard}.
                    Python FFI code is not really applicable for us.
                \item More work on \ttpink{jlrs} would be a huge benefit here.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{end}
    \centering
    
    \scalebox{2}{🛑}

    \cyan{thanks}
\end{frame}


\end{document}
