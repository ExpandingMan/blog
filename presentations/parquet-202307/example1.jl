# have been running this through REPL with iron to make example
using Parquet2, Tables, Transducers, Random, StructArrays
using Parquet2: writefile, Dataset
x = 1:10^4 |> Map() do j
    b = rand() < 0.5 ? missing : rand(1:8)
    (;a=randstring(8), b, c=randn())
end |> StructVector;
x[1:5]
writefile("example1.parq", x)
ds = Dataset("example1.parq")
ds[1]
x′ = Tables.columns(ds) |> NamedTuple |> StructVector;
x′[1:5]
